# How to run?
Pods are not included to git. You need to run `pod install` yourself, 
or run `./run` script (either from shell or finder).

# Targets & Schemes
There are 3 targets: application itself, unit tests and UI tests.

Unit tests are connected to application scheme;
UI tests are linked to separate scheme (we don't want 1minute+ of execution during unit test runs);

# Naming conventions
To make class names shorter, these conventions are put into place:

* Impl suffix -- implementaion
* VC suffix -- view controller
* VM suffix -- view model
* UC suffix -- use case
