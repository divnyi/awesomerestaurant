//
//  AwesomeRestaurantUITests.swift
//  AwesomeRestaurantUITests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import XCTest

class AwesomeRestaurantUITests: XCTestCase {
    var app: XCUIApplication!
    override func setUp() {
        continueAfterFailure = false

        self.app = XCUIApplication()
        self.app.launch()
    }

    func assertCell(_ element: XCUIElement,
                    name: String,
                    status: String,
                    sortingValue: String,
                    favouriteButtonTitle: String,
                    file: StaticString = #file, line: UInt = #line) {
        let identifiers = Constants.AcessibilityIdentifiers.RestaurantListCell.self
        
        let nameElement = element.children(matching: .staticText)[identifiers.name.rawValue]
        XCTAssertTrue(nameElement.exists, "name label doesn't exist",
                      file: file, line: line)
        XCTAssertEqual(nameElement.label, name, "name don't match",
                       file: file, line: line)

        let statusElement = element.children(matching: .staticText)[identifiers.status.rawValue]
        XCTAssertTrue(statusElement.exists, "status label doesn't exist",
                      file: file, line: line)
        XCTAssertEqual(statusElement.label, status, "status don't match",
                       file: file, line: line)
        
        let sortingValueElement = element.children(matching: .staticText)[identifiers.sortingValue.rawValue]
        XCTAssertTrue(sortingValueElement.exists, "sorting value label doesn't exist",
                      file: file, line: line)
        XCTAssertEqual(sortingValueElement.label, sortingValue, "sorting value don't match",
                       file: file, line: line)
        
        let buttonElement = element.children(matching: .button)[identifiers.favourite.rawValue]
        XCTAssertTrue(buttonElement.exists, "favourite button doesn't exist",
                      file: file, line: line)
        XCTAssertEqual(buttonElement.label, favouriteButtonTitle, "favourite button don't match",
                       file: file, line: line)
    }

    /// let's have brand new state
    func test01DeleteApp() {
        Springboard.deleteApp(appName: "AwesomeRestaurant")
    }
    
    /// check that new state is showing what we expect to see
    func test02InitialState() {
        assertCell(self.app.cells.element(boundBy: 0),
                   name: "Lunchpakketdienst",
                   status: "open",
                   sortingValue: "Best match: 306.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 1),
                   name: "De Amsterdamsche Tram",
                   status: "open",
                   sortingValue: "Best match: 304.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 2),
                   name: "CIRO 1939",
                   status: "open",
                   sortingValue: "Best match: 12.0",
                   favouriteButtonTitle: "☆")
    }

    /// click favourite, expect to see favourite on top
    func test03FavouriteButtonClick() {
        // click favourite on Indian Kitchen
        self.app.cells.element(boundBy: 3).buttons.firstMatch.tap()
        
        assertCell(self.app.cells.element(boundBy: 0),
                   name: "Indian Kitchen",
                   status: "open",
                   sortingValue: "Best match: 11.0",
                   favouriteButtonTitle: "★")
        assertCell(self.app.cells.element(boundBy: 1),
                   name: "Lunchpakketdienst",
                   status: "open",
                   sortingValue: "Best match: 306.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 2),
                   name: "De Amsterdamsche Tram",
                   status: "open",
                   sortingValue: "Best match: 304.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 3),
                   name: "CIRO 1939",
                   status: "open",
                   sortingValue: "Best match: 12.0",
                   favouriteButtonTitle: "☆")
    }
    
    /// favourites are saved between app runs
    func test04DatabasePersistanceTest() {
        assertCell(self.app.cells.element(boundBy: 0),
                   name: "Indian Kitchen",
                   status: "open",
                   sortingValue: "Best match: 11.0",
                   favouriteButtonTitle: "★")
        assertCell(self.app.cells.element(boundBy: 1),
                   name: "Lunchpakketdienst",
                   status: "open",
                   sortingValue: "Best match: 306.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 2),
                   name: "De Amsterdamsche Tram",
                   status: "open",
                   sortingValue: "Best match: 304.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 3),
                   name: "CIRO 1939",
                   status: "open",
                   sortingValue: "Best match: 12.0",
                   favouriteButtonTitle: "☆")
    }
    
    /// test that clicking on sorting options shows screen with all the sorting options
    func test05SortingOptionsScreenTransition() {
        let identifiers = Constants.AcessibilityIdentifiers.RestaurantListVC.self
        self.app.buttons[identifiers.sortingButton.rawValue].tap()
        
        XCTAssertTrue(self.app.buttons["Best match"].exists)
        XCTAssertTrue(self.app.buttons["Newest"].exists)
        XCTAssertTrue(self.app.buttons["Rating avarage"].exists)
        XCTAssertTrue(self.app.buttons["Distance"].exists)
        XCTAssertTrue(self.app.buttons["Popularity"].exists)
        XCTAssertTrue(self.app.buttons["Avarage price"].exists)
        XCTAssertTrue(self.app.buttons["Delivery costs"].exists)
        XCTAssertTrue(self.app.buttons["Minimal cost"].exists)
    }
    
    /// click sorting options, select newest, check list items
    func test06SortingSelected() {
        let identifiers = Constants.AcessibilityIdentifiers.RestaurantListVC.self
        self.app.buttons[identifiers.sortingButton.rawValue].tap()
        self.app.buttons["Newest"].tap()
        
        assertCell(self.app.cells.element(boundBy: 0),
                   name: "Indian Kitchen",
                   status: "open",
                   sortingValue: "Newest: 272.0",
                   favouriteButtonTitle: "★")
        assertCell(self.app.cells.element(boundBy: 1),
                   name: "Lunchpakketdienst",
                   status: "open",
                   sortingValue: "Newest: 259.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 2),
                   name: "Roti Shop",
                   status: "open",
                   sortingValue: "Newest: 247.0",
                   favouriteButtonTitle: "☆")
        assertCell(self.app.cells.element(boundBy: 3),
                   name: "Sushi One",
                   status: "open",
                   sortingValue: "Newest: 238.0",
                   favouriteButtonTitle: "☆")
    }
    
    /// tap on cell, check that navigation was made by comparing the title
    func test07CellTapTransition() {
        self.app.cells.firstMatch.tap()
        
        let navigationBar = self.app.navigationBars.firstMatch
        let navbarTitle = navigationBar.children(matching: .staticText).firstMatch.label
        XCTAssertEqual(navbarTitle, "Indian Kitchen")
    }
    
    /// search by some name, check list items
    func test08Search() {
        self.app.searchFields.firstMatch.tap()
        self.app.searchFields.firstMatch.typeText("Aarti")
        
        assertCell(self.app.cells.element(boundBy: 0),
                   name: "Aarti 2",
                   status: "open",
                   sortingValue: "Best match: 5.0",
                   favouriteButtonTitle: "☆")
        XCTAssertEqual(self.app.cells.count, 1)
    }
}
