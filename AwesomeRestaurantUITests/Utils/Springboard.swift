//
//  Springboard.swift
//  AwesomeRestaurantUITests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import XCTest

class Springboard {

    static let springboard = XCUIApplication(bundleIdentifier: "com.apple.springboard")

    /**
     Terminate and delete the app via springboard
     */
    class func deleteApp(appName: String) {
        XCUIApplication().terminate()

         // Force delete the app from the springboard
        let icon = springboard.icons[appName]
        if icon.exists {
            icon.press(forDuration: 1.3)

            springboard.buttons["Delete App"].tap()
            springboard.alerts.buttons["Delete"].tap()
        } else {
            XCTFail("didn't find app with name \(appName)")
        }
    }
 }
