//
//  Restaurants.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct Restaurants: Codable {
    let restaurants: [Restaurant]
}
