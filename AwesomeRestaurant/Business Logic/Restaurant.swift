//
//  Restaurant.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct Restaurant: Codable {
    enum Status: String, Codable {
        case open
        case ahead = "order ahead"
        case closed
    }
    
    let name: String
    let status: Status
    let sortingValues: SortingValues

    var isFavourite: Bool = false
    
    private enum CodingKeys: String, CodingKey {
        case name, status, sortingValues
    }
}
