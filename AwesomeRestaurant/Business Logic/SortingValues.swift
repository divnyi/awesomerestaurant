//
//  SortingValues.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct SortingValues: Codable {
    let bestMatch: Double
    let newest: Double
    let ratingAverage: Double
    let distance: Int
    let popularity: Double
    let averageProductPrice: Int
    let deliveryCosts: Int
    let minCost: Int
}
