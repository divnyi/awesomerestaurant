//
//  SortingTypeSelectionVM.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

protocol SortingTypeSelectionVM {
    var sortingTypes: [String] { get }
    
    func selectItemAt(idx: Int)
    func closeButtonClicked()
}

class SortingTypeSelectionVMImpl: SortingTypeSelectionVM {
    // MARK: inputs
    private let useCase: RestaurantListUC
    private let sortingTypeSelectionCoordinator: Coordinator
    
    init(useCase: RestaurantListUC,
         sortingTypeSelectionCoordinator: Coordinator) {
        self.useCase = useCase
        self.sortingTypeSelectionCoordinator = sortingTypeSelectionCoordinator
        
        self.update()
    }
    
    // MARK: - private variables
    let rawSortingTypes: [RestaurantListSortingType] = RestaurantListSortingType.allCases
    
    // MARK: - public
    var sortingTypes: [String] = []
    
    func selectItemAt(idx: Int) {
        if let sortingType = self.rawSortingTypes[safe: idx] {
            self.useCase.sortingType = sortingType
        }
        self.sortingTypeSelectionCoordinator.stop()
    }
    
    func closeButtonClicked() {
        self.sortingTypeSelectionCoordinator.stop()
    }
    
    // MARK: - private methods
    private func update() {
        self.sortingTypes = self.rawSortingTypes.map { $0.readableName }
    }
}
