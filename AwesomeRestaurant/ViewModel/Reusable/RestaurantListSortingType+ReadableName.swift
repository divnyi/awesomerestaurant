//
//  RestaurantListSortingType+Readable.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/12/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

extension RestaurantListSortingType {
    var readableName: String {
        switch self {
        case .averageProductPrice:
            return "Avarage price"
        case .bestMatch:
            return "Best match"
        case .deliveryCosts:
            return "Delivery costs"
        case .distance:
            return "Distance"
        case .minCost:
            return "Minimal cost"
        case .newest:
            return "Newest"
        case .popularity:
            return "Popularity"
        case .ratingAverage:
            return "Rating avarage"
        }
    }
}
