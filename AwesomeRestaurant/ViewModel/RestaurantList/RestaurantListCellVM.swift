//
//  RestaurantListCellVM.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

struct RestaurantListCellVM {
    let isFavourite: Bool
    let name: String
    let status: String
    let sortingTitle: String
    
    static func from(restaurant: Restaurant,
                     sortingType: RestaurantListSortingType) -> RestaurantListCellVM {
        let sortingValue = restaurant.sortingValues.valueForSortingType(sortingStrategy: sortingType)
        return RestaurantListCellVM(isFavourite: restaurant.isFavourite,
                                    name: restaurant.name,
                                    status: restaurant.status.rawValue,
                                    sortingTitle: "\(sortingType.readableName): \(sortingValue)")
    }
}

extension SortingValues {
    func valueForSortingType(sortingStrategy: RestaurantListSortingType) -> Any {
        switch sortingStrategy {
        case .averageProductPrice: return self.averageProductPrice
        case .bestMatch: return self.bestMatch
        case .deliveryCosts: return self.deliveryCosts
        case .distance: return self.distance
        case .minCost: return self.minCost
        case .newest: return self.newest
        case .popularity: return self.popularity
        case .ratingAverage: return self.ratingAverage
        }
    }
}
