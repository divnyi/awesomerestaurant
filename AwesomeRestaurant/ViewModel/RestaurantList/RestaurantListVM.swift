//
//  RestaurantListVM.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

protocol RestaurantListVMDelegate: AnyObject {
    func updated()
}

protocol RestaurantListVM {
    var delegate: RestaurantListVMDelegate? { get set }

    var restaurants: [RestaurantListCellVM] { get }
    var sortingTitle: String { get }

    func favouritesButtonClicked(idx: Int)
    func rowSelectedAt(idx: Int)
    func sortingOptionsButtonClicked()
    func didSearch(string: String)
}

class RestaurantListVMImpl: RestaurantListVM, RestaurantListUCDelegate {
    // MARK: inputs
    private let useCase: RestaurantListUC
    private let sortingTypeSelectionCoordinator: Coordinator
    private let restaurantDetailsCoordinator: RestaurantDetailsCoordinator
    
    init(useCase: RestaurantListUC,
         sortingTypeSelectionCoordinator: Coordinator,
         restaurantDetailsCoordinator: RestaurantDetailsCoordinator) {
        self.useCase = useCase
        self.sortingTypeSelectionCoordinator = sortingTypeSelectionCoordinator
        self.restaurantDetailsCoordinator = restaurantDetailsCoordinator
        
        self.update()
    }

    // MARK: - public
    weak var delegate: RestaurantListVMDelegate?
    var restaurants: [RestaurantListCellVM] = []
    var sortingTitle: String = ""
    
    func favouritesButtonClicked(idx: Int) {
        if let restaurant = self.useCase.restaurants[safe: idx] {
            if restaurant.isFavourite {
                self.useCase.removeFavourites(restaurants: [restaurant])
            } else {
                self.useCase.addFavourites(restaurants: [restaurant])
            }
        }
    }
    
    func rowSelectedAt(idx: Int) {
        if let restaurant = self.useCase.restaurants[safe: idx] {
            self.restaurantDetailsCoordinator.start(restaurant: restaurant)
        }
    }
    
    func sortingOptionsButtonClicked() {
        self.sortingTypeSelectionCoordinator.start()
    }
    
    func didSearch(string: String) {
        self.useCase.searchSubstring = string
    }

    // MARK: - RestaurantListUCDelegate
    func updated() {
        self.update()
    }
    
    // MARK: - private methods
    private func update() {
        self.restaurants = self.useCase.restaurants.map { restaurant in
            RestaurantListCellVM.from(restaurant: restaurant,
                                      sortingType: self.useCase.sortingType)
        }
        self.sortingTitle = "Sorted by: \(self.useCase.sortingType.readableName)"
        self.delegate?.updated()
    }
}
