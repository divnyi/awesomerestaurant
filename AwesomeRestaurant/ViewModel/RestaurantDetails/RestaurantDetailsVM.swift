//
//  RestaurantDetailsVM.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

struct RestaurantDetailsVMInput {
    let restaurant: Restaurant
}

protocol RestaurantDetailsVM {
    var title: String { get }
    var details: String { get }
}

class RestaurantDetailsVMImpl: RestaurantDetailsVM {
    // MARK: inputs
    private let restaurant: Restaurant
    
    init(input: RestaurantDetailsVMInput) {
        self.restaurant = input.restaurant
        
        self.update()
    }

    // MARK: - public
    var title: String = ""
    var details: String = ""
    
    // MARK: - private methods
    private func update() {
        self.title = self.restaurant.name
        self.details = """
\(self.restaurant.name)
is \(self.restaurant.status.rawValue)

\(RestaurantListSortingType.bestMatch.readableName): \(self.restaurant.sortingValues.bestMatch)
\(RestaurantListSortingType.newest.readableName): \(self.restaurant.sortingValues.newest)
\(RestaurantListSortingType.ratingAverage.readableName): \(self.restaurant.sortingValues.ratingAverage)
\(RestaurantListSortingType.distance.readableName): \(self.restaurant.sortingValues.distance)
\(RestaurantListSortingType.popularity.readableName): \(self.restaurant.sortingValues.popularity)
\(RestaurantListSortingType.averageProductPrice.readableName): \(self.restaurant.sortingValues.averageProductPrice)
\(RestaurantListSortingType.deliveryCosts.readableName): \(self.restaurant.sortingValues.deliveryCosts)
\(RestaurantListSortingType.minCost.readableName): \(self.restaurant.sortingValues.minCost)
"""
    }
}
