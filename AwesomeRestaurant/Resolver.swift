//
//  Resolver.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Resolver {
    // MARK: - screens
    
    // MARK: restaurant list
    lazy var restaurantListVC = { [unowned self] () -> RestaurantListVC in
        var vm = self.restaurantListVM()
        let vc = RestaurantListVC.createVC(viewModel: vm)
        vm.delegate = vc
        return vc
    }
    lazy var restaurantListVM = { [unowned self] () -> RestaurantListVM in
        var uc = self.restaurantListUC()
        let vm = RestaurantListVMImpl(
            useCase: uc,
            sortingTypeSelectionCoordinator: self.sortingTypeSelectorCoordinator,
            restaurantDetailsCoordinator: self.restaurantDetailsCoordinator
        )
        uc.delegate = vm
        return vm
    }
    lazy var restaurantListUC = { [unowned self] () -> RestaurantListUC in
        RestaurantListUCImpl(restaurantsRepository: self.restaurantsRepository())
    }
    
    // MARK: sorting type selector
    var sortingTypeSelectorCoordinator: Coordinator!

    lazy var sortingTypeSelectionVC = { [unowned self] () -> SortingTypeSelectionVC in
        let vm = self.sortingTypeSelectionVM()
        let vc = SortingTypeSelectionVC.createVC(viewModel: vm)
        return vc
    }
    lazy var sortingTypeSelectionVM = { [unowned self] () -> SortingTypeSelectionVM in
        let uc = self.restaurantListUC()
        let vm = SortingTypeSelectionVMImpl(
            useCase: uc,
            sortingTypeSelectionCoordinator: self.sortingTypeSelectorCoordinator
        )
        return vm
    }

    // MARK: restaurant details
    var restaurantDetailsCoordinator: RestaurantDetailsCoordinator!

    lazy var restaurantDetailsVC = { [unowned self] () -> RestaurantDetailsVC in
        var vm = self.restaurantDetailsVM()
        let vc = RestaurantDetailsVC.createVC(viewModel: vm)
        return vc
    }
    var restaurantDetailsVMInput: RestaurantDetailsVMInput!
    lazy var restaurantDetailsVM = { [unowned self] () -> RestaurantDetailsVM in
        RestaurantDetailsVMImpl(input: self.restaurantDetailsVMInput)
    }

    // MARK: - repository
    lazy var restaurantsRepository = { [unowned self] () -> RestaurantsRepository in
        RestaurantsRepositoryImpl(restaurantsDataSource: self.restaurantsDataSource(),
                                  favouritesDataSource: self.favouritesDataSource())
    }
    lazy var restaurantsDataSource = { () -> RestaurantsDataSource in
        RestaurantsLocalDataSource()
    }
    lazy var favouritesDataSource = { () -> FavouritesDataSource in
        FavouritesRealmDataSource()
    }
}
