//
//  AcessibilityIdentifiers.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/15/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

extension Constants {
    class AcessibilityIdentifiers {
        enum RestaurantListCell: String {
            case favourite
            case name
            case status
            case sortingValue
        }
        enum RestaurantListVC: String {
            case sortingTitle
            case sortingButton
            case searchBar
        }
    }
}
