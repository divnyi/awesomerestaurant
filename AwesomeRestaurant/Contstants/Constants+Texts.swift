//
//  Constants+Texts.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/15/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Constants {
    class Texts {
        class RestaurantListCell {
            static let favouriteButtonTrue = "★"
            static let favouriteButtonFalse = "☆"
        }
    }
}
