//
//  RestaurantDetailsCoordinator.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol RestaurantDetailsCoordinator {
    func start(restaurant: Restaurant)
}

class RestaurantDetailsCoordinatorImpl: RestaurantDetailsCoordinator {
    // MARK: inputs
    private weak var navigationViewController: UINavigationController?
    
    init(navigationViewController: UINavigationController?) {
        self.navigationViewController = navigationViewController
    }

    // MARK: RestaurantDetailsCoordinator
    func start(restaurant: Restaurant) {
        let resolver = Resolver()
        resolver.restaurantDetailsVMInput = RestaurantDetailsVMInput(
            restaurant: restaurant
        )

        let vc = resolver.restaurantDetailsVC()
        
        self.navigationViewController?.pushViewController(vc, animated: true)
    }
}
