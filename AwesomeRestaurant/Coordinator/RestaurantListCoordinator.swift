//
//  RestaurantListCoordinator.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class RestaurantListCoordinator: Coordinator {
    // MARK: inputs
    private weak var navigationViewController: UINavigationController?
    
    init(navigationViewController: UINavigationController?) {
        self.navigationViewController = navigationViewController
    }
    
    // MARK: - coordinator
    func start() {
        let resolver = Resolver()

        // all resolving of use case will yield the same object
        let restaurantListUC = resolver.restaurantListUC()
        resolver.restaurantListUC = { restaurantListUC }

        resolver.sortingTypeSelectorCoordinator = SortingTypeSelectionCoordinator(
            presentingVC: self.navigationViewController,
            restaurantListUC: restaurantListUC
        )
        resolver.restaurantDetailsCoordinator = RestaurantDetailsCoordinatorImpl(
            navigationViewController: self.navigationViewController
        )
        
        let vc = resolver.restaurantListVC()
        
        self.navigationViewController?.viewControllers = [vc]
    }
}
