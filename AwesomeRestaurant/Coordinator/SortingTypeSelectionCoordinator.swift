//
//  SortingTypeSelectorCoordinator.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class SortingTypeSelectionCoordinator: Coordinator {
    // MARK: inputs
    private weak var presentingVC: UIViewController?
    private weak var restaurantListUC: RestaurantListUC?
    
    init(presentingVC: UIViewController?,
         restaurantListUC: RestaurantListUC?) {
        self.presentingVC = presentingVC
        self.restaurantListUC = restaurantListUC
    }
    
    // MARK: - coordinator
    func start() {
        let resolver = Resolver()
        resolver.sortingTypeSelectorCoordinator = self
        if let uc = self.restaurantListUC {
            resolver.restaurantListUC = { uc }
        }

        let vc = resolver.sortingTypeSelectionVC()
        
        vc.modalPresentationStyle = .overFullScreen
        self.presentingVC?.present(vc,
                                  animated: false,
                                  completion: nil)
    }
    
    func stop() {
        self.presentingVC?.dismiss(animated: false,
                                  completion: nil)
    }
}
