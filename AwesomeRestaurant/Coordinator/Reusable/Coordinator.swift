//
//  Coordinator.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

protocol Coordinator: class {
    func start()
    func stop()
}

extension Coordinator {
    func stop() {}
}
