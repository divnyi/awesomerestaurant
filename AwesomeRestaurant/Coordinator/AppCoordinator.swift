//
//  AppCoordinator.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    // MARK: inputs
    private weak var window: UIWindow?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    // MARK: - local variables
    private var restaurantListCoordinator: RestaurantListCoordinator?

    // MARK: - coordinator
    func start() {
        let navigationController = UINavigationController()

        self.restaurantListCoordinator = RestaurantListCoordinator(
            navigationViewController: navigationController
        )

        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        self.restaurantListCoordinator?.start()
    }
}
