//
//  UIView+roundedCorners.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable
    var roundedCorners: Double {
        get {
            return Double(self.layer.cornerRadius)
        }
        set {
            self.layer.masksToBounds = newValue > 0
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
}
