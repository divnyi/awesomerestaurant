//
//  RestaurantListVC.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class RestaurantListVC: UITableViewController, UISearchResultsUpdating, RestaurantListVMDelegate {
    // MARK: inputs
    private var viewModel: RestaurantListVM!
    
    class func createVC(viewModel: RestaurantListVM) -> RestaurantListVC {
        let storyboard = UIStoryboard(name: "RestaurantList", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "RestaurantListVC") as! RestaurantListVC

        vc.viewModel = viewModel

        return vc
    }
    
    // MARK: - outlets
    @IBOutlet private weak var sortedByLabel: UILabel!
    @IBOutlet weak var sortedByButton: UIButton!
    
    // MARK: - local variables
    private let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    // MARK: - UI
    private func setupUI() {
        self.searchController.searchResultsUpdater = self
        
        // Place the search bar in the navigation item's title view.
        self.navigationItem.titleView = self.searchController.searchBar
        // Don't hide the navigation bar because the search bar is in it.
        self.searchController.hidesNavigationBarDuringPresentation = false
        // Don't dim the bg - we want search results to be clickable
        self.searchController.obscuresBackgroundDuringPresentation = false
        
        let identifiers = Constants.AcessibilityIdentifiers.RestaurantListVC.self

        self.searchController.searchBar.accessibilityIdentifier = identifiers.searchBar.rawValue
        self.sortedByLabel.accessibilityIdentifier = identifiers.sortingTitle.rawValue
        self.sortedByButton.accessibilityIdentifier = identifiers.sortingButton.rawValue

        self.updateUI()
    }
    
    private func updateUI() {
        self.sortedByLabel.text = self.viewModel.sortingTitle
        self.tableView.reloadData()
    }
    
    // MARK: - RestaurantListVMDelegate
    func updated() {
        self.updateUI()
    }
    
    // MARK: - table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.restaurants.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idx = indexPath.row
        
        let cell = tableView
            .dequeueReusableCell(withIdentifier: "RestaurantListCell", for: indexPath)
            as! RestaurantListCell
        let cellVM = self.viewModel.restaurants[idx]
        
        cell.updateWith(viewModel: cellVM)
        cell.buttonClickedCallback = { [weak self] in
            self?.viewModel.favouritesButtonClicked(idx: idx)
        }
        
        return cell
    }
    
    // MARK: - table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let idx = indexPath.row
        self.viewModel.rowSelectedAt(idx: idx)
    }
    
    // MARK: - UISearchResultsUpdating
    @available(iOS 8.0, *)
    func updateSearchResults(for searchController: UISearchController) {
        let text = searchController.searchBar.text ?? ""
        self.viewModel.didSearch(string: text)
    }
    
    // MARK: - actions
    @IBAction func sortingOptionsButtonClicked(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        self.viewModel.sortingOptionsButtonClicked()
    }
}
