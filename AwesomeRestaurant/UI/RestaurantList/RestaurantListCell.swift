//
//  RestaurantListCell.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class RestaurantListCell: UITableViewCell {
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sortingValueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let identifiers = Constants.AcessibilityIdentifiers.RestaurantListCell.self
        
        self.favouriteButton.accessibilityIdentifier = identifiers.favourite.rawValue
        self.nameLabel.accessibilityIdentifier = identifiers.name.rawValue
        self.statusLabel.accessibilityIdentifier = identifiers.status.rawValue
        self.sortingValueLabel.accessibilityIdentifier = identifiers.sortingValue.rawValue
    }
    
    func updateWith(viewModel: RestaurantListCellVM) {
        let favouriteStar = viewModel.isFavourite ?
            Constants.Texts.RestaurantListCell.favouriteButtonTrue :
            Constants.Texts.RestaurantListCell.favouriteButtonFalse
        self.favouriteButton.setTitle(favouriteStar, for: .normal)
        self.nameLabel.text = viewModel.name
        self.statusLabel.text = viewModel.status
        self.sortingValueLabel.text = viewModel.sortingTitle
    }
    
    var buttonClickedCallback: (() -> Void)?
    @IBAction func favouriteButtonClicked(_ sender: Any) {
        self.buttonClickedCallback?()
    }
}
