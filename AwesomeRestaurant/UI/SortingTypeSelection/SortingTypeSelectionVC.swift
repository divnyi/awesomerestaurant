//
//  SortingTypeSelectionVC.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class SortingTypeSelectionVC: UIViewController {
    // MARK: inputs
    private var viewModel: SortingTypeSelectionVM!
    
    class func createVC(viewModel: SortingTypeSelectionVM) -> SortingTypeSelectionVC {
        let storyboard = UIStoryboard(name: "SortingTypeSelection", bundle: Bundle.main)
        let vc = storyboard
            .instantiateViewController(withIdentifier: "SortingTypeSelectionVC")
            as! SortingTypeSelectionVC

        vc.viewModel = viewModel

        return vc
    }

    // MARK: - outlets
    @IBOutlet weak var stackView: UIStackView!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    // MARK: - UI
    private func setupUI() {
        self.edgesForExtendedLayout = []
        for (idx, sortingType) in self.viewModel.sortingTypes.enumerated() {
            let button = UIButton()
            button.setTitle(sortingType, for: .normal)
            button.addTarget(self, action: #selector(selected(sender:)), for: .touchUpInside)
            button.tag = idx
            button.setTitleColor(UIColor.black, for: .normal)
            self.stackView.addArrangedSubview(button)
        }
    }
    
    // MARK: - actions
    @IBAction func backgroundClicked(_ sender: Any) {
        self.viewModel.closeButtonClicked()
    }
    
    @objc
    func selected(sender: UIButton) {
        self.viewModel.selectItemAt(idx: sender.tag)
    }
}
