//
//  RestaurantDetailsVC.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class RestaurantDetailsVC: UIViewController {
    // MARK: inputs
    private var viewModel: RestaurantDetailsVM!
    
    class func createVC(viewModel: RestaurantDetailsVM) -> RestaurantDetailsVC {
        let storyboard = UIStoryboard(name: "RestaurantDetails", bundle: Bundle.main)
        let vc = storyboard
            .instantiateViewController(withIdentifier: "RestaurantDetailsVC")
            as! RestaurantDetailsVC

        vc.viewModel = viewModel

        return vc
    }
    
    // MARK: - outlets
    @IBOutlet weak var detailsLabel: UILabel!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    // MARK: - UI
    private func setupUI() {
        self.navigationItem.title = self.viewModel.title
        self.detailsLabel.text = self.viewModel.details
    }
}
