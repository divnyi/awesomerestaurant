//
//  RestaurantsRepository.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import Foundation

protocol RestaurantsRepository {
    func getRestaurants(callback: (Result<[Restaurant], Error>) -> Void)
    
    func addFavourites(restaurants: [Restaurant])
    func removeFavourites(restaurants: [Restaurant])
}

class RestaurantsRepositoryImpl: NSObject, RestaurantsRepository {
    private let restaurantsDataSource: RestaurantsDataSource
    private let favouritesDataSource: FavouritesDataSource
    
    init(restaurantsDataSource: RestaurantsDataSource,
         favouritesDataSource: FavouritesDataSource) {
        self.restaurantsDataSource = restaurantsDataSource
        self.favouritesDataSource = favouritesDataSource
    }
    
    func getRestaurants(callback: (Result<[Restaurant], Error>) -> Void) {
        self.restaurantsDataSource.getRestaurants { result in
            switch result {
            case .failure(let error):
                callback(.failure(error))
            case .success(let restaurants):
                self.favouritesDataSource.getFavouriteRestaurantNames { result in
                    switch result {
                    case .failure(let error):
                        callback(.failure(error))
                    case .success(let favouriteRestaurantNames):
                        let result = combine(restaurants: restaurants,
                                             favouriteRestaurantNames: favouriteRestaurantNames)
                        callback(.success(result))
                    }
                }
            }
        }
    }
    
    private func combine(restaurants: [Restaurant],
                         favouriteRestaurantNames: [String]) -> [Restaurant] {
        var restarauntDictionary: [String: Restaurant] = [:]
        for restaurant in restaurants {
            restarauntDictionary[restaurant.name] = restaurant
        }
        for restaurantName in favouriteRestaurantNames {
            restarauntDictionary[restaurantName]?.isFavourite = true
        }
        let result = Array(restarauntDictionary.values)
        return result
    }
    
    func addFavourites(restaurants: [Restaurant]) {
        self.favouritesDataSource.addFavourites(restaurantNames: restaurants.map { $0.name })
    }
    
    func removeFavourites(restaurants: [Restaurant]) {
        self.favouritesDataSource.removeFavourites(restaurantNames: restaurants.map { $0.name })
    }
}
