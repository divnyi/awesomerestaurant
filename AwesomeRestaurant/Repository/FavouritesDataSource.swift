//
//  FavouritesDataSource.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import Foundation
import RealmSwift

protocol FavouritesDataSource {
    func addFavourites(restaurantNames: [String])
    func removeFavourites(restaurantNames: [String])
    func getFavouriteRestaurantNames(callback: (Result<[String], Error>) -> Void)
}

class RealmFavourite: Object {
    @objc dynamic var name = ""
    
    override class func primaryKey() -> String? {
        return "name"
    }
    
    class func from(name: String) -> RealmFavourite {
        let favourite = RealmFavourite()
        favourite.name = name
        return favourite
    }
}

class FavouritesRealmDataSource: NSObject, FavouritesDataSource {
    private let realm = try! Realm()
    
    func addFavourites(restaurantNames: [String]) {
        try? realm.write {
            let favourites = restaurantNames.compactMap { name -> RealmFavourite? in
                if realm.object(ofType: RealmFavourite.self,
                                forPrimaryKey: name) != nil {
                    return nil
                }
                return RealmFavourite.from(name: name)
            }
            realm.add(favourites)
        }
    }
    
    func removeFavourites(restaurantNames: [String]) {
        try? realm.write {
            let favourites = restaurantNames.compactMap {
                realm.object(ofType: RealmFavourite.self,
                             forPrimaryKey: $0)
            }
            realm.delete(favourites)
        }
    }
    
    func getFavouriteRestaurantNames(callback: (Result<[String], Error>) -> Void) {
        let result = realm.objects(RealmFavourite.self)
            .map { $0.name }
        let restaurantNames = Array(result)
        callback(.success(restaurantNames))
    }
}
