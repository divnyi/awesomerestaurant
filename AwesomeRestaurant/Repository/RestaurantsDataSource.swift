//
//  RestaurantsDataSource.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import Foundation

protocol RestaurantsDataSource {
    func getRestaurants(callback: (Result<[Restaurant], Error>) -> Void)
}

class RestaurantsLocalDataSource: NSObject, RestaurantsDataSource {
    enum Errors: Error {
        case jsonReadFailed
        case parsingFailure
    }

    func getRestaurants(callback: (Result<[Restaurant], Error>) -> Void) {
        guard let jsonData = Utils.readJson(filename: "restaurants") else {
            callback(.failure(Errors.jsonReadFailed))
            return
        }
        guard let restaurants = Restaurants.decode(jsonData) else {
            callback(.failure(Errors.parsingFailure))
            return
        }
        callback(.success(restaurants.restaurants))
    }
}
