//
//  Utils.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Utils: NSObject {
    class func readJson(filename: String,
                        bundle: Bundle = Bundle.main) -> Data? {
        if let path = bundle.path(forResource: filename, ofType: "json"),
           let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe) {
            return data
        }
        return nil
    }
}
