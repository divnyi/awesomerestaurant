//
//  Decodable+decode.swift
//  AwesomeRestaraunt
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Decodable {
    static func decode(_ data: Data) -> Self? {
        return try? JSONDecoder().decode(self, from: data)
    }
}
