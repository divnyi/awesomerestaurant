//
//  FavouritesStrategy.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

class RestaurantFavouritesSplit: SplittingStrategy<Restaurant> {
    override func split(items: [Restaurant]) -> [[Restaurant]] {
        let favourites = items.filter { $0.isFavourite }
        let other = items.filter { !$0.isFavourite }
        return [favourites, other]
    }
}
