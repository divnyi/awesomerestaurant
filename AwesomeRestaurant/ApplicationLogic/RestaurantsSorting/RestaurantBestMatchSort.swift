//
//  RestaurantBestMatchSort.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

class RestaurantBestMatchSort: SortingStrategy<Restaurant> {
    override func sort(items: [Restaurant]) -> [Restaurant] {
        return items.sorted { $0.sortingValues.bestMatch > $1.sortingValues.bestMatch }
    }
}
