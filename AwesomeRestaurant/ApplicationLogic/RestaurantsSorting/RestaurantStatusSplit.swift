//
//  OpeningsSort.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

class RestaurantStatusSplit: SplittingStrategy<Restaurant> {
    override func split(items: [Restaurant]) -> [[Restaurant]] {
        let open = items.filter { $0.status == .open }
        let ahead = items.filter { $0.status == .ahead }
        let closed = items.filter { $0.status == .closed }
        return [open, ahead, closed]
    }
}
