//
//  RestaurantFilterByName.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class RestaurantSearchByName: SortingStrategy<Restaurant> {
    var searchSubstring: String = ""
    
    override func sort(items: [Restaurant]) -> [Restaurant] {
        if searchSubstring.isEmpty {
            return items
        }
        return items.filter {
            $0.name.range(of: self.searchSubstring, options: .caseInsensitive) != nil
        }
    }
}
