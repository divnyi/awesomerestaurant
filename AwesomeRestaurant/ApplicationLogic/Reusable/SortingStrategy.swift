//
//  SortStrategy.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

class SortingStrategy<Type>: SplittingStrategy<Type> {
    func sort(items: [Type]) -> [Type] { return items }
    
    override func split(items: [Type]) -> [[Type]] {
        return [self.sort(items: items)]
    }
}
