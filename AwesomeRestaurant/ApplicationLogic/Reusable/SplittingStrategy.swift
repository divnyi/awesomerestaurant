//
//  SplittingStrategy.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

class SplittingStrategy<Type> {
    func split(items: [Type]) -> [[Type]] { return [items] }
}

extension SplittingStrategy {
    static func prioritySplit(itemGroupes: [[Type]],
                              strategies: [SplittingStrategy]) -> [[Type]] {
        var itemGroupes = itemGroupes
        for strategy in strategies {
            itemGroupes = itemGroupes.flatMap { items in
                strategy.split(items: items)
            }
        }
        return itemGroupes
    }
    
    static func prioritySort(items: [Type],
                             strategies: [SplittingStrategy]) -> [Type] {
        return self.prioritySplit(itemGroupes: [items], strategies: strategies).flatMap { $0 }
    }
}
