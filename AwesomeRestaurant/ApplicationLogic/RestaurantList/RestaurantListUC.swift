//
//  RestaurantListUC.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol RestaurantListUCDelegate: AnyObject {
    func updated()
}

protocol RestaurantListUC: class {
    var delegate: RestaurantListUCDelegate? { get set }

    var restaurants: [Restaurant] { get }
    var sortingType: RestaurantListSortingType { get set }
    var searchSubstring: String { get set }
    
    func addFavourites(restaurants: [Restaurant])
    func removeFavourites(restaurants: [Restaurant])
}

class RestaurantListUCImpl: RestaurantListUC {
    // MARK: inputs
    private let repo: RestaurantsRepository
    
    init(restaurantsRepository: RestaurantsRepository) {
        self.repo = restaurantsRepository
        
        self.setup()
    }
    
    // MARK: - public
    weak var delegate: RestaurantListUCDelegate?

    var restaurants: [Restaurant] = [] {
        didSet {
            self.delegate?.updated()
        }
    }

    var sortingType: RestaurantListSortingType = .bestMatch {
        didSet {
            if oldValue == sortingType { return }
            self.sort()
        }
    }
    
    var searchSubstring: String = "" {
        didSet {
            if oldValue == searchSubstring { return }
            self.sort()
        }
    }
    
    func addFavourites(restaurants: [Restaurant]) {
        self.repo.addFavourites(restaurants: restaurants)
        self.update()
    }
    
    func removeFavourites(restaurants: [Restaurant]) {
        self.repo.removeFavourites(restaurants: restaurants)
        self.update()
    }
    
    // MARK: - variables
    private var unsortedRestaurants: [Restaurant] = [] {
        didSet {
            self.sort()
        }
    }
    
    // MARK: - private methods
    private func setup() {
        self.update()
    }
    
    private func update() {
        self.repo.getRestaurants { result in
            switch result {
            case .failure:
                // TODO: error handler
                break
            case .success(let restaurants):
                self.unsortedRestaurants = restaurants
            }
        }
    }
    
    private func sort() {
        let filter = RestaurantSearchByName()
        filter.searchSubstring = self.searchSubstring
        let sorted = SortingStrategy.prioritySort(items: self.unsortedRestaurants,
                                                  strategies: [
                                                    filter,
                                                    RestaurantFavouritesSplit(),
                                                    RestaurantStatusSplit(),
                                                    self.sortingType.sortingStrategy
        ])
        self.restaurants = sorted
    }
}
