//
//  RestaurantListSortingType.swift
//  AwesomeRestaurant
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum RestaurantListSortingType: CaseIterable {
    case bestMatch
    case newest
    case ratingAverage
    case distance
    case popularity
    case averageProductPrice
    case deliveryCosts
    case minCost
    
    var sortingStrategy: SortingStrategy<Restaurant> {
        switch self {
        case .bestMatch:
            return RestaurantBestMatchSort()
        case .newest:
            return RestaurantNewestSort()
        case .ratingAverage:
            return RestaurantRatingSort()
        case .distance:
            return RestaurantDistanceSort()
        case .popularity:
            return RestaurantPopularitySort()
        case .averageProductPrice:
            return RestaurantAvaragePriceSort()
        case .deliveryCosts:
            return RestaurantDeliveryCostSort()
        case .minCost:
            return RestaurantMinimumCostSort()
        }
    }
}
