//
//  RestaurantsData.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class RestaurantsTestData {
    static let restaurants = [
        Restaurant(name: "1",
                   status: .open,
                   sortingValues: SortingValues(
                    bestMatch: 5.0,
                    newest: 1.0,
                    ratingAverage: 1.0,
                    distance: 1,
                    popularity: 1.0,
                    averageProductPrice: 1,
                    deliveryCosts: 1,
                    minCost: 1
        )),
        Restaurant(name: "2",
                   status: .open,
                   sortingValues: SortingValues(
                    bestMatch: 1.0,
                    newest: 5.0,
                    ratingAverage: 1.0,
                    distance: 1,
                    popularity: 1.0,
                    averageProductPrice: 1,
                    deliveryCosts: 1,
                    minCost: 1
        )),
        Restaurant(name: "3",
                   status: .closed,
                   sortingValues: SortingValues(
                    bestMatch: 1.0,
                    newest: 1.0,
                    ratingAverage: 1.0,
                    distance: 1,
                    popularity: 1.0,
                    averageProductPrice: 1,
                    deliveryCosts: 1,
                    minCost: 1
        )),
        Restaurant(name: "4",
                   status: .closed,
                   sortingValues: SortingValues(
                    bestMatch: 0.0,
                    newest: 0.0,
                    ratingAverage: 0.0,
                    distance: 0,
                    popularity: 0.0,
                    averageProductPrice: 0,
                    deliveryCosts: 0,
                    minCost: 0
            ),
                   isFavourite: true),
    ]
}
