//
//  AwesomeRestaurantTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/10/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

class RestaurantsDataSourceTests: BaseTestCase {
    // MARK: target
    var jsonDataSource: RestaurantsDataSource!
    
    override func setUp() {
        super.setUp()
        
        self.jsonDataSource = self.resolver.restaurantsDataSource()
    }

    // MARK: - tests
    func testGetRestaurants() {
        self.jsonDataSource.getRestaurants { result in
            switch result {
            case .failure(let error):
                XCTFail("\(error)")
            case .success(let restaurants):
                XCTAssertEqual(restaurants.count, 19)
                
                if let first = restaurants.first {
                    XCTAssertEqual(first.name, "Tanoshii Sushi")
                    XCTAssertEqual(first.status, .open)
                    XCTAssertEqual(first.sortingValues.bestMatch, 0.0)
                    XCTAssertEqual(first.sortingValues.newest, 96.0)
                    XCTAssertEqual(first.sortingValues.ratingAverage, 4.5)
                    XCTAssertEqual(first.sortingValues.distance, 1190)
                    XCTAssertEqual(first.sortingValues.popularity, 17.0)
                    XCTAssertEqual(first.sortingValues.averageProductPrice, 1536)
                    XCTAssertEqual(first.sortingValues.deliveryCosts, 200)
                    XCTAssertEqual(first.sortingValues.minCost, 1000)
                }
            }
        }
    }
}
