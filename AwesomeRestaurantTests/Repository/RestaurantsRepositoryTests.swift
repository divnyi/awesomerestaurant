//
//  RestaurantsRepositoryTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

class RestaurantsRepositoryTests: BaseTestCase {
    // MARK: target
    var repo: RestaurantsRepository!
    // MARK: mocks
    var dataSource: RestaurantsDataSourceMock!

    override func setUp() {
        super.setUp()
        
        self.dataSource = RestaurantsDataSourceMock()
        self.resolver.restaurantsDataSource = { self.dataSource }
        
        self.repo = self.resolver.restaurantsRepository()
    }
    
    // MARK: - test data
    let sampleSortingValue = SortingValues(bestMatch: 0.0,
                                           newest: 0.0,
                                           ratingAverage: 0.0,
                                           distance: 0,
                                           popularity: 0.0,
                                           averageProductPrice: 0,
                                           deliveryCosts: 0,
                                           minCost: 0)
    
    enum SampleErrors: Error {
        case error1
        case error2
    }

    // MARK: - tests
    func testGetRestaraunts() {
        self.dataSource.restaurants = [
            Restaurant(name: "1", status: .closed, sortingValues: sampleSortingValue),
            Restaurant(name: "2", status: .closed, sortingValues: sampleSortingValue),
            Restaurant(name: "3", status: .closed, sortingValues: sampleSortingValue),
        ]
        self.favouritesDataSource.restaurantNames = [ "2" ]
        
        self.repo.getRestaurants { result in
            switch result {
            case .failure(let error):
                XCTFail("\(error)")
            case .success(let restaurants):
                XCTAssertEqual(restaurants.count, 3)
                for restaurant in restaurants {
                    if restaurant.name == "2" {
                        XCTAssertEqual(restaurant.isFavourite, true)
                    } else {
                        XCTAssertEqual(restaurant.isFavourite, false)
                    }
                }
            }
        }
    }
    
    func testGetRestarauntsError1() {
        self.dataSource.error = SampleErrors.error1
        
        self.repo.getRestaurants { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error as? SampleErrors, SampleErrors.error1)
            case .success(let restaurants):
                XCTFail("did expect failure \(restaurants)")
            }
        }
    }

    func testGetRestarauntsError2() {
        self.favouritesDataSource.error = SampleErrors.error1
        
        self.repo.getRestaurants { result in
            switch result {
            case .failure(let error):
                XCTAssertEqual(error as? SampleErrors, SampleErrors.error1)
            case .success(let restaurants):
                XCTFail("did expect failure \(restaurants)")
            }
        }
    }
    
    func testAddFavourites() {
        let restaurant = [
            Restaurant(name: "1", status: .closed, sortingValues: sampleSortingValue),
            Restaurant(name: "2", status: .closed, sortingValues: sampleSortingValue),
        ]
        
        self.repo.addFavourites(restaurants: restaurant)
        
        XCTAssertEqual(self.favouritesDataSource.added, ["1", "2"])
        XCTAssertEqual(self.favouritesDataSource.removed, nil)
    }

    func testRemoveFavourites() {
        let restaurant = [
            Restaurant(name: "1", status: .closed, sortingValues: sampleSortingValue),
            Restaurant(name: "2", status: .closed, sortingValues: sampleSortingValue),
        ]
        
        self.repo.removeFavourites(restaurants: restaurant)
        
        XCTAssertEqual(self.favouritesDataSource.added, nil)
        XCTAssertEqual(self.favouritesDataSource.removed, ["1", "2"])
    }
}
