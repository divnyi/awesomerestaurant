//
//  RestaurantListVMTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

// swiftlint:disable weak_delegate
class RestaurantListVMTests: BaseTestCase {
    // MARK: target
    var viewModel: RestaurantListVM!
    // MARK: mocks
    var restaurantDetailsCoordinator: RestaurantDetailsCoordinatorMock!
    var sortingTypeSelectorCoordinator: CoordinatorMock!
    var useCase: RestaurantListUCMock!
    var delegate: RestaurantListVMDelegateMock!
    
    override func setUp() {
        super.setUp()
        
        self.restaurantDetailsCoordinator = RestaurantDetailsCoordinatorMock()
        self.resolver.restaurantDetailsCoordinator = self.restaurantDetailsCoordinator
        
        self.sortingTypeSelectorCoordinator = CoordinatorMock()
        self.resolver.sortingTypeSelectorCoordinator = self.sortingTypeSelectorCoordinator
        
        self.useCase = RestaurantListUCMock()
        self.useCase.restaurants = self.restaurants
        self.resolver.restaurantListUC = { self.useCase }
        
        self.viewModel = self.resolver.restaurantListVM()
        
        self.delegate = RestaurantListVMDelegateMock()
        self.viewModel.delegate = self.delegate
    }
    
    // MARK: - test data
    let restaurants = RestaurantsTestData.restaurants

    // MARK: - tests
    func testInitialState() {
        XCTAssertEqual(self.viewModel.sortingTitle, "Sorted by: Best match")
        
        do {
            let vm = self.viewModel.restaurants[safe: 0]
            XCTAssertEqual(vm?.name, "1")
            XCTAssertEqual(vm?.isFavourite, false)
            XCTAssertEqual(vm?.status, "open")
            XCTAssertEqual(vm?.sortingTitle, "Best match: 5.0")
        }
        
        do {
            let vm = self.viewModel.restaurants[safe: 1]
            XCTAssertEqual(vm?.name, "2")
            XCTAssertEqual(vm?.isFavourite, false)
            XCTAssertEqual(vm?.status, "open")
            XCTAssertEqual(vm?.sortingTitle, "Best match: 1.0")
        }

        do {
            let vm = self.viewModel.restaurants[safe: 2]
            XCTAssertEqual(vm?.name, "3")
            XCTAssertEqual(vm?.isFavourite, false)
            XCTAssertEqual(vm?.status, "closed")
            XCTAssertEqual(vm?.sortingTitle, "Best match: 1.0")
        }
        
        do {
            let vm = self.viewModel.restaurants[safe: 3]
            XCTAssertEqual(vm?.name, "4")
            XCTAssertEqual(vm?.isFavourite, true)
            XCTAssertEqual(vm?.status, "closed")
            XCTAssertEqual(vm?.sortingTitle, "Best match: 0.0")
        }
        
        XCTAssertEqual(self.delegate.updatedCount, 0)
    }
    
    func testChangingSortingType() {
        self.useCase.sortingType = .newest
        
        XCTAssertEqual(self.delegate.updatedCount, 0)
        
        self.useCase.delegate?.updated()
        
        XCTAssertEqual(self.delegate.updatedCount, 1)
        
        XCTAssertEqual(self.viewModel.sortingTitle, "Sorted by: Newest")
        
        do {
            let vm = self.viewModel.restaurants[safe: 0]
            XCTAssertEqual(vm?.name, "1")
            XCTAssertEqual(vm?.isFavourite, false)
            XCTAssertEqual(vm?.status, "open")
            XCTAssertEqual(vm?.sortingTitle, "Newest: 1.0")
        }
        
        do {
            let vm = self.viewModel.restaurants[safe: 1]
            XCTAssertEqual(vm?.name, "2")
            XCTAssertEqual(vm?.isFavourite, false)
            XCTAssertEqual(vm?.status, "open")
            XCTAssertEqual(vm?.sortingTitle, "Newest: 5.0")
        }

        do {
            let vm = self.viewModel.restaurants[safe: 2]
            XCTAssertEqual(vm?.name, "3")
            XCTAssertEqual(vm?.isFavourite, false)
            XCTAssertEqual(vm?.status, "closed")
            XCTAssertEqual(vm?.sortingTitle, "Newest: 1.0")
        }
        
        do {
            let vm = self.viewModel.restaurants[safe: 3]
            XCTAssertEqual(vm?.name, "4")
            XCTAssertEqual(vm?.isFavourite, true)
            XCTAssertEqual(vm?.status, "closed")
            XCTAssertEqual(vm?.sortingTitle, "Newest: 0.0")
        }
    }
    
    func testFavouritesButtonClicked() {
        self.viewModel.favouritesButtonClicked(idx: 0)
        XCTAssertEqual(self.useCase.added?.first?.name, "1")

        self.viewModel.favouritesButtonClicked(idx: 3)
        XCTAssertEqual(self.useCase.removed?.first?.name, "4")
    }
    
    func testRowSelectedAt() {
        self.viewModel.rowSelectedAt(idx: 0)
        XCTAssertEqual(self.restaurantDetailsCoordinator.restaurants.count, 1)
        XCTAssertEqual(self.restaurantDetailsCoordinator.restaurants.first?.name,
                       self.restaurants[0].name)
    }
    
    func testSortingOptionsButtonClicked() {
        self.viewModel.sortingOptionsButtonClicked()
        XCTAssertEqual(self.sortingTypeSelectorCoordinator.startedCount, 1)
    }
    
    func testDidSearch() {
        let string = "asdf"
        self.viewModel.didSearch(string: string)
        XCTAssertEqual(self.useCase.searchSubstring, string)
    }
}
