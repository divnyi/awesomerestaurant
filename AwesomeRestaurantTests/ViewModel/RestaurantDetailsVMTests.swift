//
//  RestaurantDetailsVMTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

class RestaurantDetailsVMTests: BaseTestCase {
    // MARK: target
    var viewModel: RestaurantDetailsVM!
    
    override func setUp() {
        super.setUp()
        
        self.resolver.restaurantDetailsVMInput = .init(
            restaurant: RestaurantsTestData.restaurants[0]
        )
        
        self.viewModel = self.resolver.restaurantDetailsVM()
    }
    
    func testInitilState() {
        XCTAssertEqual(self.viewModel.title, "1")
        XCTAssertEqual(self.viewModel.details, """
1
is open

Best match: 5.0
Newest: 1.0
Rating avarage: 1.0
Distance: 1
Popularity: 1.0
Avarage price: 1
Delivery costs: 1
Minimal cost: 1
""")
    }
}
