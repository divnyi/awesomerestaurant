//
//  SortingTypeSelectionVMTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

class SortingTypeSelectionVMTests: BaseTestCase {
    // MARK: target
    var viewModel: SortingTypeSelectionVM!
    // MARK: mocks
    var sortingTypeSelectorCoordinator: CoordinatorMock!
    var useCase: RestaurantListUCMock!
    
    override func setUp() {
        super.setUp()
        
        self.sortingTypeSelectorCoordinator = CoordinatorMock()
        self.resolver.sortingTypeSelectorCoordinator = self.sortingTypeSelectorCoordinator
        
        self.useCase = RestaurantListUCMock()
        self.resolver.restaurantListUC = { self.useCase }
        
        self.viewModel = self.resolver.sortingTypeSelectionVM()
    }
    
    // MARK: - tests
    func testInitialState() {
        let expected = [
            "Best match",
            "Newest",
            "Rating avarage",
            "Distance",
            "Popularity",
            "Avarage price",
            "Delivery costs",
            "Minimal cost"
        ]
        
        XCTAssertEqual(self.viewModel.sortingTypes.count, expected.count)
        for (idx, item) in self.viewModel.sortingTypes.enumerated() {
            XCTAssertEqual(item, expected[safe: idx])
        }
        XCTAssertEqual(self.sortingTypeSelectorCoordinator.stopCount, 0)
    }
    
    func testSelectItemAt() {
        self.viewModel.selectItemAt(idx: 3)
        
        XCTAssertEqual(self.useCase.sortingType, .distance)
        XCTAssertEqual(self.sortingTypeSelectorCoordinator.stopCount, 1)
    }
    
    func testCloseButtonClicked() {
        self.viewModel.closeButtonClicked()
        
        XCTAssertEqual(self.sortingTypeSelectorCoordinator.stopCount, 1)
    }
}
