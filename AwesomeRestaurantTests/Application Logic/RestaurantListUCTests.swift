//
//  RestaurantListUCTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

// swiftlint:disable weak_delegate
class RestaurantListUCTests: BaseTestCase {
    // MARK: target
    var useCase: RestaurantListUC!
    // MARK: mocks
    var dataSource: RestaurantsDataSourceMock!
    var delegate: RestaurantListUCDelegateMock!
    
    override func setUp() {
        super.setUp()
        
        self.dataSource = RestaurantsDataSourceMock()
        self.resolver.restaurantsDataSource = { self.dataSource }
        
        self.dataSource.restaurants = self.restaurants
        
        self.useCase = self.resolver.restaurantListUC()
        
        self.delegate = RestaurantListUCDelegateMock()
        self.useCase.delegate = self.delegate
    }
    
    // MARK: - test data
    let restaurants = RestaurantsTestData.restaurants
    
    // MARK: - tests
    func assertMatchesNameList(_ list: [String],
                               file: StaticString = #file, line: UInt = #line) {
        XCTAssertEqual(self.useCase.restaurants.count, list.count,
                       file: file, line: line)
        
        for (idx, name) in list.enumerated() {
            XCTAssertEqual(self.useCase.restaurants[safe: idx]?.name, name,
                           "\(idx)",
                           file: file, line: line)
        }
    }
    
    func testInitialState() {
        assertMatchesNameList(["4", "1", "2", "3"])
        XCTAssertEqual(self.delegate.updatedCount, 0)
    }
    
    func testSetSortingType() {
        self.useCase.sortingType = .newest
        assertMatchesNameList(["4", "2", "1", "3"])
        XCTAssertEqual(self.delegate.updatedCount, 1)
    }
    
    func testSearchSubstring() {
        self.useCase.searchSubstring = "2"
        assertMatchesNameList(["2"])
        XCTAssertEqual(self.delegate.updatedCount, 1)

        self.useCase.searchSubstring = "4"
        assertMatchesNameList(["4"])
        XCTAssertEqual(self.delegate.updatedCount, 2)
    }
    
    func testAddFavourites() {
        let list = [self.restaurants[0]]
        self.useCase.addFavourites(restaurants: list)
        XCTAssertEqual(self.favouritesDataSource.added, list.map { $0.name })
        XCTAssertEqual(self.delegate.updatedCount, 1)
    }
    
    func testRemoveFavourites() {
        let list = [self.restaurants[0]]
        self.useCase.removeFavourites(restaurants: list)
        XCTAssertEqual(self.favouritesDataSource.removed, list.map { $0.name })
        XCTAssertEqual(self.delegate.updatedCount, 1)
    }
}
