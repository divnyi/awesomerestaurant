//
//  RestaurantsSortingTests.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

class RestaurantsSortingTests: BaseTestCase {
    var jsonDataSource: RestaurantsDataSource!
    
    override func setUp() {
        super.setUp()
        
        self.jsonDataSource = self.resolver.restaurantsDataSource()
    }
    
    private func getRestaurants(file: StaticString = #file, line: UInt = #line) -> [Restaurant] {
        var returnValue: [Restaurant] = []
        self.jsonDataSource.getRestaurants { result in
            switch result {
            case .failure(let error):
                XCTFail("\(error)", file: file, line: line)
            case .success(let restaurants):
                returnValue = restaurants
            }
        }
        return returnValue
    }

    func testPrioritySort() {
        var restaurants = self.getRestaurants()
        
        restaurants[2].isFavourite = true
        restaurants[3].isFavourite = true
        restaurants[5].isFavourite = true
        
        let sortedRestaurants = SortingStrategy.prioritySort(
            items: restaurants,
            strategies: [
                RestaurantFavouritesSplit(),
                RestaurantStatusSplit(),
                RestaurantBestMatchSort()
            ]
        )
        
        XCTAssertEqual(sortedRestaurants[0].name, "Aarti 2")    // favourite, open, bestMatch == 5
        XCTAssertEqual(sortedRestaurants[1].name, "Sushi One")  // favourite, open, bestMatch == 3
        XCTAssertEqual(sortedRestaurants[2].name, "Royal Thai") // favourite, ahead
        XCTAssertEqual(sortedRestaurants[3].name, "Lunchpakketdienst") // non-favourite, open, top bestMatch
    }
    
    func testFavouritesSplit() {
        var restaurants = self.getRestaurants()
        
        restaurants[1].isFavourite = true
        restaurants[3].isFavourite = true
        restaurants[5].isFavourite = true
        
        let sortedRestaurants = RestaurantFavouritesSplit().split(items: restaurants)
        XCTAssertEqual(sortedRestaurants.count, 2)
        
        guard let favourite = sortedRestaurants[safe: 0],
            let other = sortedRestaurants[safe: 1] else {
                return
        }
        XCTAssertEqual(favourite.count, 3)
        
        XCTAssertEqual(favourite[safe:0]?.name, restaurants[1].name)
        XCTAssertEqual(favourite[safe:1]?.name, restaurants[3].name)
        XCTAssertEqual(favourite[safe:2]?.name, restaurants[5].name)
        
        XCTAssertEqual(other[safe:0]?.name, restaurants[0].name)
        XCTAssertEqual(other[safe:1]?.name, restaurants[2].name)
        XCTAssertEqual(other[safe:2]?.name, restaurants[4].name)
        XCTAssertEqual(other[safe:3]?.name, restaurants[6].name)
        XCTAssertEqual(other[safe:4]?.name, restaurants[7].name)
        XCTAssertEqual(other[safe:5]?.name, restaurants[8].name)
    }
    
    func testStateSplit() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantStatusSplit().split(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, 3)
        
        XCTAssertEqual(sortedRestaurants[safe:0]?.count, 8)
        XCTAssertEqual(sortedRestaurants[safe:1]?.count, 7)
        XCTAssertEqual(sortedRestaurants[safe:2]?.count, 4)
        
        for restaurant in sortedRestaurants[safe:0] ?? [] {
            XCTAssertEqual(restaurant.status, .open)
        }
        for restaurant in sortedRestaurants[safe:1] ?? [] {
            XCTAssertEqual(restaurant.status, .ahead)
        }
        for restaurant in sortedRestaurants[safe:2] ?? [] {
            XCTAssertEqual(restaurant.status, .closed)
        }
    }
    
    func testBestMatchSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.bestMatch.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertGreaterThanOrEqual(first.sortingValues.bestMatch,
                                            second.sortingValues.bestMatch,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testNewestSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.newest.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertGreaterThanOrEqual(first.sortingValues.newest,
                                            second.sortingValues.newest,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testRatingSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.ratingAverage.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertGreaterThanOrEqual(first.sortingValues.ratingAverage,
                                            second.sortingValues.ratingAverage,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testDistanceSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.distance.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertLessThanOrEqual(first.sortingValues.distance,
                                         second.sortingValues.distance,
                                         "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testPopularitySort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.popularity.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertGreaterThanOrEqual(first.sortingValues.popularity,
                                            second.sortingValues.popularity,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testAvaragePriceSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.averageProductPrice.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertLessThanOrEqual(first.sortingValues.averageProductPrice,
                                            second.sortingValues.averageProductPrice,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testDeliverCostSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.deliveryCosts.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertLessThanOrEqual(first.sortingValues.deliveryCosts,
                                            second.sortingValues.deliveryCosts,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testMinimumCostSort() {
        let restaurants = self.getRestaurants()
        let sortedRestaurants = RestaurantListSortingType.minCost.sortingStrategy
            .sort(items: restaurants)
        
        XCTAssertEqual(sortedRestaurants.count, restaurants.count)
        
        for idx in 0..<restaurants.count-1 {
            if let first = sortedRestaurants[safe: idx],
                let second = sortedRestaurants[safe: idx+1] {
                XCTAssertLessThanOrEqual(first.sortingValues.minCost,
                                            second.sortingValues.minCost,
                                            "comparing [\(idx)] with [\(idx+1)]")
            }
        }
    }
    
    func testSearchByName() {
        let restaurants = self.getRestaurants()
        let filter = RestaurantSearchByName()
        filter.searchSubstring = "shi On"
        let sortedRestaurants = filter.sort(items: restaurants)

        XCTAssertEqual(sortedRestaurants.count, 1)
        XCTAssertEqual(sortedRestaurants.first?.name, "Sushi One")
    }
    
    func testSearchByEmptyName() {
        let restaurants = self.getRestaurants()
        let filter = RestaurantSearchByName()
        filter.searchSubstring = ""
        let sortedRestaurants = filter.sort(items: restaurants)

        XCTAssertEqual(sortedRestaurants.count, 19)
    }
}
