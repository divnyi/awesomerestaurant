//
//  MockedRestaurantDetailsCoordinator.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class RestaurantDetailsCoordinatorMock: RestaurantDetailsCoordinator {
    var restaurants: [Restaurant] = []
    func start(restaurant: Restaurant) {
        self.restaurants += [restaurant]
    }
}
