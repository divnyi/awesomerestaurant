//
//  RestaurantListUCMock.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class RestaurantListUCMock: RestaurantListUC {
    weak var delegate: RestaurantListUCDelegate?

    var restaurants: [Restaurant] = []
    var sortingType: RestaurantListSortingType = .bestMatch
    var searchSubstring: String = ""
    
    var added: [Restaurant]?
    func addFavourites(restaurants: [Restaurant]) {
        self.added = restaurants
    }

    var removed: [Restaurant]?
    func removeFavourites(restaurants: [Restaurant]) {
        self.removed = restaurants
    }
}
