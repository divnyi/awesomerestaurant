//
//  RestaurantsDataSourceMock.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class RestaurantsDataSourceMock: RestaurantsDataSource {
    var restaurants: [Restaurant] = []
    var error: Error?
    func getRestaurants(callback: (Result<[Restaurant], Error>) -> Void) {
        if let error = error {
            callback(.failure(error))
        } else {
            callback(.success(self.restaurants))
        }
    }
}
