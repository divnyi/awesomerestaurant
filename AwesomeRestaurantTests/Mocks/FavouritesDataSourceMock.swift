//
//  FavouritesDataSourceMock.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class FavouritesDataSourceMock: FavouritesDataSource {
    var added: [String]?
    func addFavourites(restaurantNames: [String]) {
        self.added = restaurantNames
    }
    
    var removed: [String]?
    func removeFavourites(restaurantNames: [String]) {
        self.removed = restaurantNames
    }
    
    var restaurantNames: [String] = []
    var error: Error?
    func getFavouriteRestaurantNames(callback: (Result<[String], Error>) -> Void) {
        if let error = error {
            callback(.failure(error))
        } else {
            callback(.success(self.restaurantNames))
        }
    }
}
