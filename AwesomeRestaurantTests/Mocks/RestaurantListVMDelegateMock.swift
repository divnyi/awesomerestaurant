//
//  RestaurantListVMDelegateMock.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class RestaurantListVMDelegateMock: RestaurantListVMDelegate {
    var updatedCount = 0
    func updated() {
        self.updatedCount += 1
    }
}
