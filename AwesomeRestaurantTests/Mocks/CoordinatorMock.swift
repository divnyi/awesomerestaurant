//
//  MockedCoordinator.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/14/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

class CoordinatorMock: Coordinator {
    var startedCount = 0
    func start() {
        self.startedCount += 1
    }
    
    var stopCount = 0
    func stop() {
        self.stopCount += 1
    }
}
