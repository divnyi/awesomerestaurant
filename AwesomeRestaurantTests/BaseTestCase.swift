//
//  BaseTestCase.swift
//  AwesomeRestaurantTests
//
//  Created by Oleksii Horishnii on 12/11/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable
import AwesomeRestaurant

import XCTest

class BaseTestCase: XCTestCase {
    let resolver = Resolver()
    
    var favouritesDataSource: FavouritesDataSourceMock!
    override func setUp() {
        super.setUp()
        
        self.favouritesDataSource = FavouritesDataSourceMock()
        self.resolver.favouritesDataSource = { self.favouritesDataSource }
    }
}
